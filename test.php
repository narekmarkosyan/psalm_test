<?php
error_reporting(E_ALL);
include __DIR__ . '/vendor/autoload.php';

$word = new \App\Office\MyWord();

$size = $word->getDefaultFontSize();

echo "Font size: {$size}\n";